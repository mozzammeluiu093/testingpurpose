<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!--  <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>-->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else

                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="" data-toggle="modal" data-target="#jobModal"><i class="fa fa-briefcase" aria-hidden="true"></i> Job</a>
                            </li>
                        
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>

                </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>



    <div id="jobModal" class="modal fade" role="dialog" style="margin-top:10%;">
            <div class="modal-dialog">
        
              <!--Job Modal content-->
              <div class="modal-content">
        
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                  </div>
        
                  <div class="modal-body">
                    <p> 
                      <label><input type="radio" name="job_post_option" value="1">Available for job</label> 
                      <br>
                      <label><input type="radio" name="job_post_option" value="2">Want to hire</label>
                    </p>
                  </div>
        
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
        
              </div>
        
            </div>
          </div>



          <!-- If available for job is selected -->
<div id="available_job_modal" class="modal fade" role="dialog" style="margin-top:10%;">
        <div class="modal-dialog">
      
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" >
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Available for job =><strong>Enter Info here</strong></h4>
                </div>
      
              <div class="modal-body">
                     <?php
                        $professions = array(
                            "CSE",
                            "EEE",
                            "BBA",
                            "MBA",
                            "MSCSE"
                        );
      
                        $locations = array(
                          "Dhaka",
                          "Chittagong",
                          "Comilla",
                          "Sylhet",
                          "Rangpur"
                      );
      
                         $positions = array(
                          "Manager",
                          "HR",
                          "Executive"  
                       );
                     ?>
                        <form class="form-horizontal"  method="post" action="" enctype="multipart/form-data">
                     
                                      {{ csrf_field() }}
                              <input type="hidden" name="_method" value="post">
      
      
                              
                              <!--Position start -->
                              <div class="form-group">
                              <label for="position" class="col-md-4 control-label">Preferred Job Position<span class="required">*</span></label>
                                     <div class="col-md-6" >
                                          <select class="form-control" name="position"  value="{{ old('position') }}" >
                                              <span class="caret"></span>
                                              @foreach( $positions as $position)
                                              <option>{{ $position }}</option>
                                              @endforeach
                                          </select>
                                     </div>
                              </div>       
                              <!--Position end -->
                            
                             
                              <!--Profession start -->
                              <div class="form-group">
                              <label for="profession" class="col-md-4 control-label">Profession<span class="required">*</span></label>
                                     <div class="col-md-6">
                                          <select class="form-control" name="profession"  value="{{ old('profession') }}">
                                              <span class="caret"></span>
                                              @foreach( $professions as $profession)
                                              <option>{{ $profession }}</option>
                                              @endforeach
                                          </select>
                                     </div><!--End of col-md-6-->
                              </div>       
                              <!--Profession end -->
                                     
                              
                              <!--cv start -->
                              <div class="form-group">
                                    <label for="CV" class="col-md-4 control-label">CV <span class="required">*</span></label>
                                    <div class="col-md-6">
                                      <input type="file" name="attachment" required>
                                      <h6>(Maximum 2MB)*</h6>
                                    </div>
                              </div>
                              <!--cv end-->
                                     
                              
                              <!-- highlight start-->
                              <div class="form-group">
                                      <label for="highlight" class="col-md-4 control-label">Highlight </label>
      
                                      <div class="col-md-6">
                                          <input placeholder="anything to highlight??" id="highlight" type="text" class="form-control" 
                                          name="highlight" value=""  autofocus  style="width:110px;">
                                      </div>
                              </div>
                              <!--highlight end-->
      
                                     
                              <!--location start-->
                              <div class="form-group">
                              <label for="location" class="col-md-4 control-label">Prefered Location<span class="required">*</span></label>
                                    <div class="col-md-6">
                                          <select class="form-control" name="location"  value="{{ old('location') }}">
                                              <span class="caret"></span>
                                              @foreach( $locations as $location)
                                              <option>{{ $location }}</option>
                                              @endforeach
                                          </select>
                                    </div>
                                </div>
                              <!--location end-->
      
                                        
                                
      
                </div><!--Available for job End of modal body-->
      
                          <div class="modal-footer">
                              <div class="col-md-6 col-md-offset-4 pull-right">
                                 <button type="submit" class="btn btn-primary">
                                   Done
                                 </button>
                              </div>
                          </div>
                         
      
                      </form><!--Available for job End of form-->
            </div><!--Available for job End of modal content-->
        </div><!--Available for job End of modal-dialog-->
      </div><!--Available for job End of modal fade-->

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
   
    <script>
            $(document).ready(function () {
                $("input[name='job_post_option']").click(function () {
                    var radioValue = $("input[name='job_post_option']:checked").val();
                    if (radioValue == 1) {
                        //setTimeout($('#jobModal').modal('hide'), 10000000);
                        $('#jobModal').modal('hide');
                        $('#available_job_modal').modal('show');
                    }
    
    
                    if (radioValue == 2) {
                        //setTimeout($('#jobModal').modal('hide'), 10000000);
                        $('#jobModal').modal('hide');
                        $('#want_to_hire_modal').modal('show');
    
                    }
                });
    
            });
    </script>
</body>
</html>
